<?php
session_start();
$departmentArray = array("EMPTY" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học dữ liệu");
$numberStudent = 0;
$listNameStudent = array("1" => "Nguyễn Văn A", "2" => "Trần Thị B", "3" => "Nguyễn Hoàng C", "4" => "Đinh Quang D");
$listDepartmentStudent = array("1" => "MAT", "2" => "MAT", "3" => "KDL", "4" => "KDL");
?>

<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="list-student.css">
</head>

<body>
    <script>
        function clearValue() {
            keyWord = document.getElementById('keyWord').value;
           
            var e = document.getElementById("select-item");
            var value = e.value;

            if (keyWord !== "") {
                document.getElementById('keyWord').value = "";
            }
            if (value !== "EMPTY") {
                document.getElementById("select-item").value = "EMPTY"
            }
           
        }
    </script>
    
  <?php 
    if(isset($_POST['btn-search']) ){
        if(isset($_POST['department'])){
            $_SESSION['department'] = $_POST['department'];
        }
        if(isset($_POST['keyWord'])){
            $_SESSION['keyWord'] = $_POST['keyWord'];
        }
    }
  
  ?>

    <div class="list-student-content">
        <div class="table-1">
            <table id="table-content1">

                <form method="post" >
                    <tr>

                        <td style="width: 20%;">
                            <label id="label">Khoa</label>
                        </td>
                        <td style="width: 80%;">
                            <select name="department" id="select-item" onchange="return chan">
                                <?php
                                foreach ($departmentArray as $key => $value) {

                                    echo "<option  value='$key'";
                                    if ($_SESSION['department'] == $key) {
                                        echo " selected";
                                    }
                                    echo ">$value</option>";
                                }
                                ?>
                              
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><label id="label">Từ khóa</label></td>
                        <td>
                            <input type='text' id='keyWord' name="keyWord" value=
                            "<?php if(isset($_SESSION['keyWord'])){
                                echo $_SESSION['keyWord'];
                            } echo "";?>"/>
                           
                        </td>
                    </tr>
                    <tr>
                        <td >
                        <input type='button' id="btn-remove" class='btn-box' value="Xóa" name="btn-remove" onclick="return clearValue();">
                        </td>
                        <td>
                        <input type='submit' id="btn-search" class='btn-box' value="Tìm kiếm" name="btn-search">
                        </td>
                    </tr>

                    <?php
                    
                    ?>
                </form>
            </table>
        </div>
        


        <div class="number-student">
            <?php
            $numberStudent = count($listNameStudent);
            echo "Số sinh viên tìm thấy: $numberStudent"
            ?>
            <form id="btn-add" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">

                <input type='submit' class='btn-box' value="Thêm" name="btn-add">

                <?php
                if (isset($_POST["btn-add"])) {
                    header('location: register.php');
                }
                ?>
            </form>

        </div>



        <div class="table-2">
            <table id="table-content2">
                <tr class="label-content">
                    <th class="no">No</th>
                    <th class="name-student">Tên sinh viên</th>
                    <th class="department">Khoa</th>
                    <th class="action">Action</th>
                </tr>
                <?php
                foreach ($listNameStudent as $key => $value) {
                    $keyDepartMent = $listDepartmentStudent[$key];
                    echo ("  
                        <tr>
                            <td class=\"no\">$key</td>
                                <td class=\"name-student\">$value</td>
                                <td class=\"department\">$departmentArray[$keyDepartMent]</td>
                                <td>
                                            
                                    <div class=\"btn-action-content\">
                                        <form>
                                            <input type=\"button\" value=\"Xóa\" id=\"btn-action\">
                                            <input type=\"button\" value=\"Sửa\" id=\"btn-action\">
                                        </form>
                                    </div>
                    
                            </td>
                                
                            
                            
                        </tr>
                    ");
                }
                ?>

            </table>
        </div>
    </div>

</body>

</html>